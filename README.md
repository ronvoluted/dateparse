# AHK v2 DateParse

Parse date and time formats to YYYYMMDDHH24MISS value.

## Usage
```autohotkey
#include DateParse.ahk
parsedDate := DateParse("12:11 PM 27-September-1994") ; "199409271211"

MsgBox "Bananaphone's release date in ISO 8601: " . FormatTime(parsedDate, "yyyy-MM-dd")
```

## Syntax
```autohotkey
DateParse(dateString, americanOrder := false)
```

## Examples
```autohotkey
DateParse("2:35 PM, 27 November, 2007") ; "200711271435"
DateParse("4:55 am Feb 27, 2004") ; "200402270455"
DateParse("Mon, 17 Aug 2009 13:23:33 GMT") ; "20090817132333"
DateParse("07 Mar 2009 13:43:58") ; "20090307134358"
DateParse("2007-06-26T14:09:12Z") ; "20070626140912"
DateParse("2007-06-25 18:52") ; "200706251852"
DateParse("19/2/05") ; "20050219"
DateParse("10/12/2007") ; "20071210"
DateParse("3/15/2009") ; "20090315"
DateParse("05-Jan-00") ; "20000105"
DateParse("Jan-05-00") ; "20000105"
DateParse("Dec-31-13") ; "20131231"
DateParse("Wed 6/27/2007") ; "20070627"
DateParse("May1960") ; "19600501"
DateParse("25May1960") ; "19600525"
DateParse("201710") ; "20171001"
```
For American date order, set second argument to `true`:
```autohotkey
DateParse("08/07/19", true) ; "20190807"
```

If date is omitted, current day will be prefixed:
```autohotkey
DateParse("1532") ; "YYYYMMDD1532"
DateParse("11:26") ; "YYYYMMDD1126"
DateParse("2:35 PM") ; "YYYYMMDD1435"
DateParse("11:22:24 AM") ; "YYYYMMDD112224"
```

### Remarks

#### Partial date returns
- No month: nothing
- No year and no day: nothing
- Time and no day: nothing
- Month and year without time: substitute 1st for day
- Day and month: substitute current year
- No date and time still substitutes current date

#### Interpretations
- Allow no separator around named months (e.g. 25May60)
- Only alphabetic month name follow on characters to prevent month taking first 2 digits of 4 digit year if there are no separators. (e.g. in 25May1960 year group only gets 60 and becomes 2060)
- Separators relaxed. Can be any character except letter or digit
- Search for named months first to prevent number month incorrectly matching in "Feb 12 11" as day =12 month=11 and skipping named month match
- With named months day or year are optional
- If numeric month is > 12 and day is <= 12, swap month and day (likely an American date)

## History
2007-06-07 **philz** creates [InvFormattime](https://autohotkey.com/board/topic/18295-invformattime/)

2007-06-23 **polyethene** (**Titan**) creates [DateParse](https://autohotkey.com/board/topic/18760-date-parser-convert-any-date-format-to-yyyymmddhh24miss/) inspired by InvFormattime 

2007-06-25 Several contributors report bugs, make suggestions and create modifications: **Ace_NoOne**, **ManaUser**, **empyrean5**, **JDP**, **alpha**, **formivore**, **Dougal**

2017-11-01 **hoppfrosch** ports [DateParse to AHK v2](https://www.autohotkey.com/boards/viewtopic.php?f=6&t=39256) based on Dougal's version and adds some functionality

2019-08-22 **Coderooney** (**ronCYA**) ports hoppfrosch's version to [new AHK v2 alpha](https://www.autohotkey.com/boards/viewtopic.php?t=67317)

## Changelog
**2019-08-22**
- Ported syntax that was incompatible with new AHK v2 alpha
- Added parentheses and comment to clarify ternary operator with 2 colons on line 76 `? : :`
- Amended formatting, grammar and spelling

## License
[CC0 1.0 Universal (CC0 1.0)
Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)