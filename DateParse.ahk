; Parse date and time formats to YYYYMMDDHH24MISS value
; https://gitlab.com/roncya/dateparse

DateParse(str, americanOrder := false) {
	; Definition of several RegExes
	static monthNames := "(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[a-zA-Z]*"
		, dayAndMonth := "(\d{1,2})[^a-zA-Z0-9:.]+(\d{1,2})"
		, dayAndMonthName := "(?:(?<Month>" . monthNames . ")[^a-zA-Z0-9:.]*(?<Day>\d{1,2})[^a-zA-Z0-9]+|(?<Day>\d{1,2})[^a-zA-Z0-9:.]*(?<Month>" . monthNames . "))"
		, monthNameAndYear := "(?<Month>" . monthNames . ")[^a-zA-Z0-9:.]*(?<Year>(?:\d{4}|\d{2}))"

	if RegExMatch(str, "i)^\s*(?:(\d{4})([\s\-:\/])(\d{1,2})\2(\d{1,2}))?(?:\s*[T\s](\d{1,2})([\s\-:\/])(\d{1,2})(?:\6(\d{1,2})\s*(?:(Z)|(\+|\-)?(\d{1,2})\6(\d{1,2})(?:\6(\d{1,2}))?)?)?)?\s*$", i) { ; ISO 8601 timestamps
		year := i[1], month := i[3], day := i[4], hour := i[5], minute := i[7], second := i[8]
		; year := i.1, month := i.3, day := i.4, hour := i.5, minute := i.7, second := i.8
	}
	else if !RegExMatch(str, "^\W*(?<Hour>\d{1,2}+)(?<Minute>\d{2})\W*$", t){ ; NOT timestring only eg 1535
		; Attempt to extract the time parts
		FoundPos := RegExMatch(str, "i)(\d{1,2})" ; hours
				. "\s*:\s*(\d{1,2})"                  ; minutes
				. "(?:\s*:\s*(\d{1,2}))?"             ; seconds
				. "(?:\s*([ap]m))?", timepart)        ; am/pm
		if (FoundPos) {
			; Time is already parsed correctly from striing
			hour := timepart[1]
			minute := timepart[2]
			second := timepart[3]
			ampm:= timepart[4]
			; Remove time to parse the date part only
			str := StrReplace(str, timepart)
		}
		; Handle remaining string without time and attempt to extract date
		if RegExMatch(str, "Ji)" . dayAndMonthName . "[^a-zA-Z0-9]*(?<Year>(?:\d{4}|\d{2}))?", d) { ; named month eg 22May14; May 14, 2014; 22May, 2014
			year := d.Year, month := d.Month, day := d.Day
		}
		else if RegExMatch(str, "i)" . monthNameAndYear, d) { ; named month and year without day eg May14; May 2014
			year := d.Year, month := d.Month
		}
		else if RegExMatch(str, "i)" . "^\W*(?<Year>\d{4})(?<Month>\d{2})\W*$", d) { ; month and year as digit only eg 201710
			year := d.Year, month := d.Month
		}
		else {
			if RegExMatch(str, "i)(\d{4})[^a-zA-Z0-9:.]+" . dayAndMonth, d) { ; 2004/22/03
				year := d[1], month := d[3], day := d[2]
			}
			else if RegExMatch(str, "i)" . dayAndMonth . "(?:[^a-zA-Z0-9:.]+((?:\d{4}|\d{2})))?", d) { ; 22/03/2004 or 22/03/04
				year := d[3], month := d[2], day := d[1]
			}
			if (RegExMatch(day, monthNames) or americanOrder and !RegExMatch(month, monthNames) or (month > 12 and day <= 12)) { ; attempt to infer day/month order
				tmp := month, month := day, day := tmp
			}
		}
	}
	else if RegExMatch(str, "^\W*(?<Hour>\d{1,2}+)(?<Minute>\d{2})\W*$", timepart){ ; timestring only eg 1535
		hour := timepart.hour
		minute := timepart.minute
	}
	if (day or month or year) and not (day and month and year) { ; partial date
		if not month or not (day or month) or (hour and not day) { ; partial date must have month and day with time or day or year without time
			return
		}
		else if not day { ; without time use 1st for day if not present
			day := 1
		}
	}

	; Format the single parts
	oYear := (StrLen(year) == 2 ? "20" . year : (year ? year : A_YYYY))
	oYear := Format("{:02.0f}", oYear)
	
	oMonth := ((month := month is "number" ? month : InStr(monthNames, SubStr(month, 1, 3)) // 4 ) > 0 ? month + 0.0 : A_MM)
	oMonth := Format("{:02.0f}", oMonth)
	
	oDay := ((day is "number") ? day : A_DD) 
	oDay := Format("{:02.0f}", oDay)
	
	if (hour != "") {
		oHour := hour + (hour == 12 ? (ampm = "am" ? -12.0 : 0.0) : (ampm = "pm" ? 12.0 : 0.0)) ; Not quaternary operator but nested ternary operators
		oHour := Format("{:02.0f}", oHour)
	
		if (minute != "") {
			oMinute := minute + 0.0
			oMinute := Format("{:02.0f}", oMinute)

			if (second != "") {
				oSecond := second + 0.0
				oSecond := Format("{:02.0f}", oSecond)
			}
		}
	}
	
	d := oYear . oMonth . oDay . oHour . oMinute . oSecond 
	return d
}